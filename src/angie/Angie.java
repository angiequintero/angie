/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package angie;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author administrator
 */
public class Angie {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        LCD lcd = new LCD();
        String valorDigitado = "1,1";
        Scanner teclado= new Scanner(System.in);
        List<String> listaNumerosPantalla = new ArrayList<String>();;
/*        listaNumerosPantalla.add("5,1");
        listaNumerosPantalla.add("5,2");
        listaNumerosPantalla.add("5,3");
        listaNumerosPantalla.add("5,4");
        listaNumerosPantalla.add("5,5");
        listaNumerosPantalla.add("5,6");
        listaNumerosPantalla.add("5,7");
        listaNumerosPantalla.add("5,8");
        listaNumerosPantalla.add("5,9");
        listaNumerosPantalla.add("5,0"); */
             
        while(!"0,0".equals(valorDigitado)){
            System.out.print("Digite el valor de entrada: ");
            valorDigitado = teclado.next();
            String[] listaDigitos = valorDigitado.split(",");
            Integer size = Integer.parseInt(listaDigitos[0]);
            Integer numeroPantalla = Integer.parseInt(listaDigitos[1]);
            if(size >= 1 && size <= 10){
                listaNumerosPantalla.add(valorDigitado);
            }else{
                System.out.println("El primer numero debe ser mayor a 1 y menor a 10");
                break;
            }
        }        
        for(int i = 0; i < listaNumerosPantalla.size(); i++){
            String[] listaDigitos = listaNumerosPantalla.get(i).split(",");
            Integer size = Integer.parseInt(listaDigitos[0]);
            Integer numeroPantalla = Integer.parseInt(listaDigitos[1]);
            String numeroImpreso = lcd.ToLCD(numeroPantalla, size);
            System.out.print(numeroImpreso);
        }
    }
    
}
