/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package angie;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author administrator
 */
public class LCD {
    /*
    private static final String _NONE = "   ";
    
	private static String _LEFT = "  |";        
	private static String _MIDL = " _ ";
	private static String _MDLT = " _|";
	private static String _MDRT = "|_ ";
	private static String _FULL = "|_|";
	private static String _BOTH = "| |"; */

	private static Map<Integer, String[]> _SEGMENTS_FOR = null;

	public static String ToLCD(int number, int size) {
            String linea_ = "";
            String espacio = "";
            String _none = "";
            String _left = "";
            String _midl = "";
            String _mdlt = "";            
            String _mdrt = "";
            String _full = "";
            String _both = "";
            
            for(int i = 1; i <= size; i++){
                linea_ += "_";
                espacio += " ";
            }
            
            _none = espacio;
            for(int i = 1; i <= size; i++){
                
                if(i == 1){
                    _midl += " ";                    
                }                
                
                _midl += "_";
                
                if(i >= 1 && i <= size-1){
                    _left += espacio + " |\n";
                    _mdlt += " " + espacio + "|\n";
                    _mdrt +=  "|" + espacio + " \n";
                    _full += "|" + espacio + "|\n";
                    _both += "|" + espacio + "|\n";
                }
                
                if(i == size){
                    _left += espacio + " |";
                    _midl += " ";
                    
                    _mdlt += " " + linea_ + "|";
                    _mdrt +=  "|" + linea_ + " ";
                    _full += "|"+ linea_ + "|";
                    _both += "|" + espacio + "|";
                }
            }
            final String _NONE = _none;
            final String _LEFT = _left;        
            final String _MIDL = _midl;
            final String _MDLT = _mdlt;
            final String _MDRT = _mdrt;
            final String _FULL = _full;
            final String _BOTH = _both;
            
            _SEGMENTS_FOR = new HashMap<Integer, String[]>() {
                {
                    put(new Integer(1), new String[] { _NONE, _LEFT, _LEFT });
                    put(new Integer(2), new String[] { _MIDL, _MDLT, _MDRT });
                    put(new Integer(3), new String[] { _MIDL, _MDLT, _MDLT });
                    put(new Integer(4), new String[] { _NONE, _FULL, _LEFT });
                    put(new Integer(5), new String[] { _MIDL, _MDRT, _MDLT });
                    put(new Integer(6), new String[] { _MIDL, _MDRT, _FULL });
                    put(new Integer(7), new String[] { _MIDL, _LEFT, _LEFT });
                    put(new Integer(8), new String[] { _MIDL, _FULL, _FULL });
                    put(new Integer(9), new String[] { _MIDL, _FULL, _MDLT });
                    put(new Integer(0), new String[] { _MIDL, _BOTH, _FULL });
                }
            };

            String[][] segments = getSegmentsForEachDigit(number);
            String[] result = Utils.arrangeHorizontally(segments);
            return toTextLines(result);
	}

	private static String toTextLines(String[] result) {
		return Utils.join(result, '\n');
	}

	private static String[][] getSegmentsForEachDigit(int number) {
		String digits = Integer.toString(number);
		String[][] result = new String[digits.length()][];
		for (int digitIndex = 0; digitIndex < digits.length(); digitIndex++) {
			result[digitIndex] = segmentsFor(digitAt(digits, digitIndex));
		}
		return result;
	}

	private static int digitAt(String digits, int i) {
		return Integer.parseInt(Character.toString(digits.charAt(i)));
	}

	private static String[] segmentsFor(int number) {
		String result[] = _SEGMENTS_FOR.get(new Integer(number));
		if (null == result)
			throw new RuntimeException(String.format("Dígito %d no encontrado",
					number));
		return result;
	}
}


class Utils {
	public static String join(String[] strings, char delim) {
		StringBuffer sb = new StringBuffer();
		for (String string : strings) {
			if (sb.length() > 0)
				sb.append(delim);
			sb.append(string);
		}
		return sb.toString();
	}

	public static String[] arrangeHorizontally(String[][] data) {
		assert data.length > 0;

		String[] result = data[0].clone();
		for (int row = 1; row < data.length; row++) {
			for (int col = 0; col < data[row].length; col++)
				result[col] += data[row][col];
		}
		return result;
	}
}
